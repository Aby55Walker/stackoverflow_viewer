// primitives
const isFetching: boolean = true
const int: number = 4
const float: number = 3
const meeage: string = 'message'
// Arrays
const numberArray: number[] = [1, 2, 3]
const numArray2: Array<number> = [1, 2, 3]
const words: string[] = ['hello', 'typescript']

// Tuple
const contact: [string, number] = ['vladilen', 123123]

// Any
let variable: any = 42
variable = 'new string'

// Functions
function sayName(name: string): string {
  console.log(name)
  return name
}
sayName('Maksim')

// Type
type Login = string

const login: Login = 'asd'

type ID = string | number
const id1: ID = 1
const id2: ID = 'asd'
// const id3: ID = false
type SomeType = string | null | undefined

// Interfaces ===============
interface Rect {
  readonly id: string
  color?: string
  size: {
    width: number
    height: number
  }
}

const rect1: Rect = {
  id: '1234',
  size: {
    width: 40,
    height: 30,
  },
  color: '#eee',
}

const rect2: Rect = {
  id: '12',
  size: {
    width: 12,
    height: 20,
  },
}
rect2.color = 'black'
// rect2.id = '123'

const rect3 = {} as Rect

// Наследование интерфейса ===========

interface RectWithArea extends Rect {
  getArea: () => number
}

const rect5: RectWithArea = {
  id: '123',
  size: {
    width: 10,
    height: 10,
  },
  getArea(): number {
    return this.size.width * this.size.height
  },
}

// ==========

interface IClock {
  time: Date
  setTime(date: Date): void
}

class Clock implements IClock {
  time: Date = new Date()

  setTime(date: Date) {
    this.time = date
  }
}

interface IStyles {
  [key: string]: string
}

// // =========
const css: IStyles = {
  border: '1px solid red',
  marginTop: '2px',
  borderRadius: '5px',
}

// Enum

enum Membership {
  Simple,
  Standart,
  Premium,
}

const membership = Membership.Standart
const membershipReverse = Membership[2]
console.log(membershipReverse)

enum SocialMedia {
  VK = 'VK',
  FACEBOOK = 'FACENBOOK',
  INSTAGRAM = 'INSTAGRAM',
}

// functions

function add(a: number, b: number): number {
  return a + b
}
function toUppercase(str: string): string {
  return str.trim().toUpperCase()
}

interface MyPosition {
  x: number | undefined
  y: number | undefined
}
interface MyPositionWithDefault extends MyPosition {
  default: string
}

function position(): MyPosition
function position(a: number): MyPositionWithDefault
function position(a: number, b: number): MyPosition

function position(a?: number, b?: number) {
  if (!a && !b) {
    return { x: undefined, y: undefined }
  }
  if (a && !b) {
    return { x: a, y: undefined, default: a.toString() }
  }
  return { x: a, y: b }
}

// console.log('Empty: ', position())
// console.log('One param: ', position(42))
// console.log('Two params: ', position(10, 15))

// classes
class Typescript {
  version: string

  constructor(version: string) {
    this.version = version
  }

  info(name: string) {
    return `[${name}: Typescript version is ${this.version}]`
  }
}

// class Car {
//   readonly model: string
//   readonly numberOfWheels: number = 4
//
//   constructor(theModel: string) {
//     this.model = theModel
//   }
// }

class Car {
  readonly numberOfWheels: number = 4

  constructor(readonly model: string) {}
}
// ========

class Animal {
  protected voice: string = ''

  public color: string = 'black'

  private go() {
    console.log('go')
  }
}

class Cat extends Animal {
  public setVoice(voice: string): void {
    this.voice = voice
  }

  public roar() {
    console.log(this.voice)
  }
}

const cat = new Cat()
cat.setVoice('meow')
console.log(cat.roar())

// ============ Абстрактные классы

abstract class Component {
  abstract render(): void

  abstract info(): string
}

class appComponent extends Component {
  render(): void {
    console.log('render')
  }

  info(): string {
    return 'this is info'
  }
}

// guards

function strip(x: string | number) {
  if (typeof x === 'number') {
    return x.toFixed()
  }
  return x.trim()
}

class MyResponse {
  header = 'response header'

  result = 'response result'
}

class MyError {
  header = 'error header'

  message = 'error message'
}
function handle(res: MyResponse | MyError) {
  if (res instanceof MyResponse) {
    return {
      info: res.header + res.result,
    }
  }
  return {
    info: res.header + res.message,
  }
}

type AlertType = 'success' | 'danger' | 'warning'

function setAlertType(type: AlertType) {
  // ...
}

setAlertType('success')
setAlertType('warning')
setAlertType('danger')

// generic

const arrayOfNumbers: Array<number> = [1, 1, 2, 3, 5, 8, 13]
const arrayOfStrings: Array<string> = ['Hello', 'world']

function reverse<T>(array: T[]): T[] {
  return array.reverse()
}

console.log(reverse(arrayOfNumbers))
reverse(arrayOfStrings)

interface Person {
  name: string
  age: number
}

type PersonKeys = keyof Person // string | number
let myName: PersonKeys = 'name'
myName = 'age'

type User = {
  _id: number
  name: string
  email: string
  createdAt: Date
}

type UserKeysNoMeta1 = Exclude<keyof User, '_id' | 'createdAt'> // name | email
type UserKeysNoMeta2 = Pick<User, 'name' | 'email'> // name | email
let u1: UserKeysNoMeta1 = 'email'
u1 = 'email'
const u2: UserKeysNoMeta2 = 'name'
