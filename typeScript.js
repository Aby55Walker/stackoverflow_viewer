var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
// primitives
var isFetching = true;
var int = 4;
var float = 3;
var meeage = 'message';
// Arrays
var numberArray = [1, 2, 3];
var numArray2 = [1, 2, 3];
var words = ['hello', 'typescript'];
// Tuple
var contact = ['vladilen', 123123];
// Any
var variable = 42;
variable = 'new string';
// Functions
function sayName(name) {
    console.log(name);
    return name;
}
sayName('Maksim');
var login = 'asd';
var id1 = 1;
var id2 = 'asd';
var rect1 = {
    id: '1234',
    size: {
        width: 40,
        height: 30
    },
    color: '#eee'
};
var rect2 = {
    id: '12',
    size: {
        width: 12,
        height: 20
    }
};
rect2.color = 'black';
// rect2.id = '123'
var rect3 = {};
var rect5 = {
    id: '123',
    size: {
        width: 10,
        height: 10
    },
    getArea: function () {
        return this.size.width * this.size.height;
    }
};
var Clock = /** @class */ (function () {
    function Clock() {
        this.time = new Date();
    }
    Clock.prototype.setTime = function (date) {
        this.time = date;
    };
    return Clock;
}());
// // =========
var css = {
    border: '1px solid red',
    marginTop: '2px',
    borderRadius: '5px'
};
// Enum
var Membership;
(function (Membership) {
    Membership[Membership["Simple"] = 0] = "Simple";
    Membership[Membership["Standart"] = 1] = "Standart";
    Membership[Membership["Premium"] = 2] = "Premium";
})(Membership || (Membership = {}));
var membership = Membership.Standart;
var membershipReverse = Membership[2];
console.log(membershipReverse);
var SocialMedia;
(function (SocialMedia) {
    SocialMedia["VK"] = "VK";
    SocialMedia["FACEBOOK"] = "FACENBOOK";
    SocialMedia["INSTAGRAM"] = "INSTAGRAM";
})(SocialMedia || (SocialMedia = {}));
// functions
function add(a, b) {
    return a + b;
}
function toUppercase(str) {
    return str.trim().toUpperCase();
}
function position(a, b) {
    if (!a && !b) {
        return { x: undefined, y: undefined };
    }
    if (a && !b) {
        return { x: a, y: undefined, "default": a.toString() };
    }
    return { x: a, y: b };
}
// console.log('Empty: ', position())
// console.log('One param: ', position(42))
// console.log('Two params: ', position(10, 15))
// classes
var Typescript = /** @class */ (function () {
    function Typescript(version) {
        this.version = version;
    }
    Typescript.prototype.info = function (name) {
        return "[" + name + ": Typescript version is " + this.version + "]";
    };
    return Typescript;
}());
// class Car {
//   readonly model: string
//   readonly numberOfWheels: number = 4
//
//   constructor(theModel: string) {
//     this.model = theModel
//   }
// }
var Car = /** @class */ (function () {
    function Car(model) {
        this.model = model;
        this.numberOfWheels = 4;
    }
    return Car;
}());
// ========
var Animal = /** @class */ (function () {
    function Animal() {
        this.voice = '';
        this.color = 'black';
    }
    Animal.prototype.go = function () {
        console.log('go');
    };
    return Animal;
}());
var Cat = /** @class */ (function (_super) {
    __extends(Cat, _super);
    function Cat() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Cat.prototype.setVoice = function (voice) {
        this.voice = voice;
    };
    Cat.prototype.roar = function () {
        console.log(this.voice);
    };
    return Cat;
}(Animal));
var cat = new Cat();
cat.setVoice('meow');
console.log(cat.roar());
