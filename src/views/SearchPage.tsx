import React from 'react'
import Search from '../components/Search/Search'

const SearchPage: React.FC = () => (
  <div className='page'>
    <Search />
  </div>
)

export default SearchPage
