import React, { useEffect } from 'react'
import { withRouter, useParams } from 'react-router-dom'
import { useDispatch, useSelector } from 'react-redux'
import { search, setSearchValue } from '../store/search/actions'
import Table from '../components/Table/Table'
import { RootState } from '../store/rootReducer'
import { AppDispatch } from '../store/types/actionTypes'
import RadioGroup, { BtnList } from '../components/RadioGroup/RadioGroup'

const list: BtnList[] = [
  {
    name: 'sort',
    value: 'votes',
    id: 1,
    text: 'votes',
  },
  {
    name: 'sort',
    value: 'activity',
    id: 2,
    text: 'activity',
  },
  {
    name: 'sort',
    value: 'creation',
    id: 3,
    text: 'creation',
  },
]

const SearchResultsPage: React.FC = () => {
  const searchResults = useSelector((state: RootState) => state.search.searchResults)

  const headers = ['Author', 'Theme', 'Answers', 'Tags']
  const dispatch: AppDispatch = useDispatch()
  const loading = useSelector((state: RootState) => state.search.loading)

  const { query } = useParams()

  const changeHandler = (value: string) => {
    dispatch(
      search({
        searchValue: query,
        sort: value,
      })
    )
  }

  useEffect(() => {
    dispatch(
      search({
        searchValue: query,
        sort: list[0].value,
      })
    )
    dispatch(setSearchValue(query))
  }, [])

  return (
    <div className='page'>
      <h3>Sort table by:</h3>
      <RadioGroup list={list} onChange={changeHandler} />
      <Table loading={loading} headers={headers} posts={searchResults} />
    </div>
  )
}

export default withRouter(SearchResultsPage)
