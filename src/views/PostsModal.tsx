import React from 'react'
import { Route } from 'react-router-dom'

import UserQuestionsModal from '../components/UserQuestionsModal/UserQuestionsModal'
import Modal from '../components/Modal/Modal'

const PostsModal: React.FC = () => {
  return (
    <Route
      path='/*/modal/:type/:query'
      render={() => {
        return (
          <Modal>
            <UserQuestionsModal />
          </Modal>
        )
      }}
    />
  )
}

export default PostsModal
