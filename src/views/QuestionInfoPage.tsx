import React, { useEffect } from 'react'
import { useParams } from 'react-router-dom'

import { useDispatch, useSelector } from 'react-redux'
import Loader from '../components/Loader/Loader'
import { setLoading } from '../store/actions'
import QuestionPage from '../components/QuestionPage/QuestionPage'
import { RootState } from '../store/rootReducer'
import { fetchQuestionData } from '../store/questionPage/actions'
import { AppDispatch } from '../store/types/actionTypes'

const QuestionInfoPage: React.FC = () => {
  const { answers, question } = useSelector((state: RootState) => state.question)
  const { questionID } = useParams()
  const dispatch: AppDispatch = useDispatch()
  const loading = useSelector((state: RootState) => state.app.loading)

  const fetchQuestionInfo = async () => {
    dispatch(setLoading(true))
    dispatch(fetchQuestionData(questionID)).then(() => dispatch(setLoading(false)))
  }

  useEffect(() => {
    if (questionID) {
      fetchQuestionInfo()
    }
  }, [])

  return <div className='page'>{loading ? <Loader /> : <QuestionPage answers={answers} questionInfo={question} />}</div>
}

export default QuestionInfoPage
