import React from 'react'
import Question from '../Question/Question'
import './QuestionPage.scss'
import Answer from '../Answer/Answer'
import { IAnswer, IQuestion } from '../../store/types/actionTypes'

interface QuestionPageProps {
  questionInfo: IQuestion
  answers: IAnswer[]
}

const QuestionPage = ({ questionInfo, answers = [] }: QuestionPageProps) => {
  return (
    <div className='question-page'>
      <div className='question-page__question'>
        <Question questionInfo={questionInfo} />
      </div>

      <div className='question-page__answers'>
        {answers.length ? (
          <>
            <h2 className='question-page__answers-title'>Answers:</h2>
            {answers.map((answer) => (
              <Answer answer={answer} key={answer.owner.name} />
            ))}
          </>
        ) : (
          <h2>No one answered this</h2>
        )}
      </div>
    </div>
  )
}

export default QuestionPage
