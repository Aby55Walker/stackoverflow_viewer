import React from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { Link, useHistory } from 'react-router-dom'
import { FaSearch } from 'react-icons/fa'

import { setSearchValue } from '../../store/search/actions'
import './Search.scss'
import { RootState } from '../../store/rootReducer'

export default function Search() {
  const dispatch = useDispatch()
  const history = useHistory()
  const searchValue = useSelector((state: RootState) => state.search.searchValue)

  const changeHandler = (e: React.ChangeEvent<HTMLInputElement>) => {
    dispatch(setSearchValue(e.target.value))
  }

  const test = (e: React.FormEvent) => {
    e.preventDefault()
    history.push(`/search-results/${searchValue}`)
  }

  return (
    <form className='search' onSubmit={test} action=''>
      <label className='search__label' htmlFor='search-input'>
        <input className='search__input' type='search' value={searchValue} onChange={changeHandler} placeholder='Search...' id='search-input' />
        <span className='search__label-text'>Type your question here</span>

        <button className='search__btn' type='submit' disabled={searchValue.length < 2}>
          <FaSearch />
        </button>
      </label>
    </form>
  )
}
