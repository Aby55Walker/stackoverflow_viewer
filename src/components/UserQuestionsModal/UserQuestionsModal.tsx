import React, { useEffect, useState } from 'react'
import { useParams } from 'react-router-dom'
import { useDispatch, useSelector } from 'react-redux'
import { AppDispatch } from '../../store/types/actionTypes'
import { RootState } from '../../store/rootReducer'
import { fetchPosts } from '../../store/modalPosts/actions'
import Table from '../Table/Table'

const UserQuestionsModal: React.FC = () => {
  const { type, query } = useParams()
  const [loading, setLoading] = useState(false)
  const dispatch: AppDispatch = useDispatch()
  const posts = useSelector((state: RootState) => state.posts.posts)

  useEffect(() => {
    setLoading(true)
    dispatch(
      fetchPosts({
        type,
        value: query,
        sort: 'votes',
      })
    ).then(() => setLoading(false))
  }, [query])

  const headers = ['Author', 'Theme', 'Answers', 'Tags']

  return <Table loading={loading} headers={headers} posts={posts} />
}

export default UserQuestionsModal
