import React from 'react'
import './Question.scss'
import { IQuestion } from '../../store/types/actionTypes'

type QuestionProps = {
  questionInfo: IQuestion
}

export default function Question({ questionInfo }: QuestionProps) {
  return (
    <div className='question'>
      <h3 className='question__author'>
        <span>Author: </span>
        {questionInfo.owner && questionInfo.owner.name}
      </h3>
      <h2 className='question__title'>{questionInfo.title}</h2>
      <div className='question__body' dangerouslySetInnerHTML={{ __html: questionInfo.body }} />
    </div>
  )
}
