import React from 'react'
import './User.scss'
import { Link } from 'react-router-dom'

export type UserProps = {
  name: string
  userID: number
  image: string
}

const User = ({ name, userID, image }: UserProps) => {
  return (
    <Link className='user' title={`open ${name} posts`} to={`/search-results/modal/userPosts/${userID}`}>
      <img className='user__img' src={image} alt='' />
      <span className='user__name link'>{name}</span>
    </Link>
  )
}

export default User
