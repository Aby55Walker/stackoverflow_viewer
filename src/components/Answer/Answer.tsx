import React from 'react'
import './Answer.scss'
import { IAnswer } from '../../store/types/actionTypes'

interface AnswerProps {
  answer: IAnswer
}

const Answer = ({ answer }: AnswerProps) => {
  return (
    <div className='answer'>
      <h3 className='answer__author'>
        <span>Author:</span>
        {answer.owner.name}
      </h3>
      <div className='answer__body' dangerouslySetInnerHTML={{ __html: answer.body }} />
    </div>
  )
}

export default Answer
