import './Table.scss'
import React from 'react'
import TableHeader from '../TableHeader/TableHeader'
import TableBody from '../TableBody/TableBody'
import { IPost } from '../../store/types/actionTypes'

interface TableProps {
  posts: IPost[]
  headers: string[]
  loading: boolean
}

const Table: React.FC<TableProps> = ({ posts, headers = [], loading }: TableProps) => {
  return (
    <table className='table'>
      <TableHeader headers={headers} />
      <TableBody posts={posts} loading={loading} />
    </table>
  )
}

export default Table
