import React from 'react'
import { Link } from 'react-router-dom'
import { IPost } from '../../store/types/actionTypes'
import User from '../User/User'

interface TableRowProps {
  posts: IPost
}

const TableRow: React.FC<TableRowProps> = ({ posts }: TableRowProps) => {
  return (
    <tr>
      <td>
        <User name={posts.owner.name} userID={posts.owner.id} image={posts.owner.image} />
      </td>

      <td>
        <Link to={`/question-info/${posts.questionID}`} title='open question page'>
          {posts.title}
        </Link>
      </td>

      <td>
        <Link to={`/question-info/${posts.questionID}`} title='open question page'>
          {posts.answerCount}
        </Link>
      </td>

      <td>
        {posts.tags.map((tag: string) => {
          return (
            <div key={tag}>
              <Link title={`open questions with "${tag}" tag`} to={`/search-results/modal/tags/${tag}`}>
                {tag}
              </Link>
            </div>
          )
        })}
      </td>
    </tr>
  )
}

export default TableRow
