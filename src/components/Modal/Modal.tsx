import React, { useEffect, useState, useRef } from 'react'
import { createPortal } from 'react-dom'
import { useHistory } from 'react-router-dom'
import { disableBodyScroll, clearAllBodyScrollLocks } from 'body-scroll-lock'
import { CSSTransition } from 'react-transition-group'
import './Modal.scss'

interface ModalProps {
  children: React.ReactNode
}

export default function Modal({ children }: ModalProps) {
  const history = useHistory()
  const [modalIsOpen, setModalIsOpen] = useState<boolean>(true)

  const modal: React.RefObject<any> | null = useRef(null)
  const modalRoot: HTMLElement | null = document.querySelector('#modal-root')

  useEffect(() => {
    if (modal) {
      disableBodyScroll(modal.current, {
        reserveScrollBarGap: true,
      })
    }
    return () => clearAllBodyScrollLocks()
  }, [])

  const closeModal = () => setModalIsOpen(false)

  const keyboardClose = (e: KeyboardEvent) => {
    if (e.key === 'Escape' || e.key === 'Backspace') {
      setModalIsOpen(false)
    }
  }

  // close modal on Escape or Backspace
  useEffect(() => {
    window.addEventListener('keydown', keyboardClose)
    return () => window.removeEventListener('keydown', keyboardClose)
  }, [])

  return createPortal(
    <CSSTransition in={modalIsOpen} timeout={300} classNames='fade' unmountOnExit appear onExited={() => history.goBack()}>
      <div ref={modal} role='dialog' aria-modal='true' className='modal'>
        <div role='dialog' id='nameInput' onClick={closeModal} className='modal__overlay' />
        <div className='modal__content'>{children}</div>
      </div>
    </CSSTransition>,
    // @ts-ignore
    modalRoot
  )
}
