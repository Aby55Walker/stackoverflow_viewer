import React from 'react'

import './TableBody.scss'

import { useSelector } from 'react-redux'
import TableRow from '../TableRow/TableRow'
import Loader from '../Loader/Loader'
import { IPost } from '../../store/types/actionTypes'
import { RootState } from '../../store/rootReducer'

interface TableBodyProps {
  posts: IPost[]
  loading: boolean
}

const TableBody: React.FC<TableBodyProps> = ({ posts, loading }: TableBodyProps) => {
  const searchValue = useSelector((state: RootState) => state.search.searchValue)

  if (loading) {
    return (
      <tbody>
        <tr>
          <td colSpan={4}>
            <Loader />
          </td>
        </tr>
      </tbody>
    )
  }

  return (
    <tbody>
      {posts.length ? (
        posts.map((post: IPost) => <TableRow key={post.questionID} posts={post} />)
      ) : (
        <tr>
          <td colSpan={4} style={{ textAlign: 'center' }}>
            <h2>
              No results on request:
              {searchValue}
            </h2>
          </td>
        </tr>
      )}
    </tbody>
  )
}

export default TableBody
