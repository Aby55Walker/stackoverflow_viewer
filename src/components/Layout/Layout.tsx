import React from 'react'
import './Layout.scss'
import Header from '../Header/Header'

interface LayoutProps {
  children: React.ReactNode
}

const Layout = ({ children }: LayoutProps) => {
  return (
    <>
      <Header />
      <main className='content'>
        <div className='container'>{children}</div>
      </main>
    </>
  )
}

export default Layout
