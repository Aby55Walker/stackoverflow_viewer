import React from 'react'
import './Loader.scss'

const Loader = () => {
  return (
    <div className='loader__wrapper'>
      <div className='lds-dual-ring' />
    </div>
  )
}

export default Loader
