import React from 'react'
import './TableHeader.scss'

interface TableHeaderProps {
  headers: string[]
}

const TableHeader: React.FC<TableHeaderProps> = ({ headers }: TableHeaderProps) => {
  return (
    <thead className='table-header'>
      <tr>
        {headers.map((title, i) => (
          <th key={i}>{title}</th>
        ))}
      </tr>
    </thead>
  )
}

export default TableHeader
