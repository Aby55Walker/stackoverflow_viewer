import React from 'react'
import './Header.scss'
import { Link } from 'react-router-dom'

const Header: React.FC = () => {
  return (
    <header className='header'>
      <div className='container'>
        <Link className='header__link' to='/'>
          StackOverflow Viewer
        </Link>
      </div>
    </header>
  )
}

export default Header
