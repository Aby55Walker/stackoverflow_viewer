import React, { useState } from 'react'

import './RadioGroup.scss'

import RadioBtn from './includes/RadioBtn'

export type BtnList = {
  name: string
  text: string
  id: number
  value: string
}

export type RadioGroupProps = {
  list: BtnList[]
  onChange: (value: string) => void
}

const RadioGroup = ({ list, onChange }: RadioGroupProps) => {
  const [selected, setSelected] = useState(list[0].value)

  const changeHandler = (value: string) => {
    setSelected(value)
    onChange(value)
  }

  return (
    <div className='radio-group'>
      {list.map(({ value, name, text, id }) => (
        <RadioBtn key={id} name={name} value={value} onChange={changeHandler} text={text} selected={selected} />
      ))}
    </div>
  )
}

export default RadioGroup
