import React from 'react'
import classNames from 'classnames'

import './RadioBtn.scss'

export type RadioBtnProps = {
  name: string
  text: string
  value: string
  selected: string
  onChange: (value: string) => void
}

const RadioBtn: React.FC<RadioBtnProps> = ({ name, text, value, selected, onChange }: RadioBtnProps) => {
  const isSelected = selected === value

  const radioBtnClasses = classNames('radio-btn', {
    active: isSelected,
  })
  const flagClasses = classNames('radio-btn__flag', {
    active: isSelected,
  })

  const keyPressHandler = (e: React.KeyboardEvent<HTMLLabelElement>) => {
    if (e.key === 'Enter') {
      onChange(value)
    }
  }

  return (
    // eslint-disable-next-line jsx-a11y/no-noninteractive-tabindex
    <label tabIndex={0} onKeyPress={keyPressHandler} className={radioBtnClasses}>
      <input className='radio-btn__input' type='radio' value={value} onChange={() => onChange(value)} checked={isSelected} name={name} />
      <div className={flagClasses} />
      <span className='dario-btn__text'>{text}</span>
    </label>
  )
}

export default RadioBtn
