import axios from 'axios'
import { IAnswer, IPost, IQuestion } from '../store/types/actionTypes'

class Api {
  async fetch(url: string): Promise<any[]> {
    const response: any = await axios.get(url)
    return response.data.items
  }

  async posts(url: string): Promise<IPost[]> {
    const items: any = await this.fetch(url)
    return items.map((item: any) => {
      return {
        owner: {
          id: item.owner.user_id,
          image: item.owner.profile_image,
          name: item.owner.display_name,
        },
        questionID: item.question_id,
        title: item.title,
        answerCount: item.answer_count,
        tags: item.tags,
      }
    })
  }

  async question(url: string): Promise<IQuestion> {
    const questionData: any = await this.fetch(url)
    console.log(questionData, 'questionData')
    return {
      body: questionData[0].body,
      owner: {
        name: questionData[0].owner.display_name,
      },
      title: questionData[0].title,
    }
  }

  async answers(url: string): Promise<IAnswer[]> {
    const answersData: any = await this.fetch(url)
    console.log(answersData)
    return answersData.map((answer: any) => ({
      body: answer.body,
      owner: {
        name: answer.owner.display_name,
      },
    }))
  }
}

export default new Api()
