import { setLoading } from '../actions'
import { FETCH_POSTS } from '../actionTypes'
import Api from '../../api/fetcher'
import { IAction } from '../types/actionTypes'

interface fetchPostsArgs {
  type: string
  value: string
  sort: string
}

export const fetchPosts = ({ type = 'userPosts', value, sort = 'votes' }: fetchPostsArgs) => {
  return async (dispatch: (action: IAction) => void) => {
    const url =
      type === 'userPosts'
        ? `users/${value}/questions?order=desc&sort=${sort}&site=stackoverflow&key=2SJ*wXuAaD5J1AYVaZgerA((`
        : `tags/${value}/faq?site=stackoverflow&key=2SJ*wXuAaD5J1AYVaZgerA((`

    try {
      dispatch(setLoading(true))
      const items = await Api.posts(url)
      dispatch({
        type: FETCH_POSTS,
        payload: items,
      })
      dispatch(setLoading(false))
    } catch (e) {
      console.warn(e.message)
      dispatch(setLoading(false))
    }
  }
}
