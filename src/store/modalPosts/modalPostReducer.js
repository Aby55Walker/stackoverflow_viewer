import { FETCH_POSTS } from '../actionTypes'

const initialState = {
  posts: [],
}

export const modalPostsReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_POSTS:
      return {
        ...state,
        posts: action.payload,
      }
    default:
      return state
  }
}
