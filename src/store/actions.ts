import { SET_LOADING } from './actionTypes'

export const setLoading = (payload: boolean) => {
  return {
    type: SET_LOADING,
    payload,
  }
}
