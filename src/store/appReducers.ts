import { SET_LOADING } from './actionTypes'
import { IAction } from './types/actionTypes'

interface AppState {
  [key: string]: boolean
}

const initialState = {
  loading: false,
}

export const appReducer = (state = initialState, action: IAction): AppState => {
  switch (action.type) {
    case SET_LOADING: {
      return {
        ...state,
        loading: action.payload,
      }
    }
    default:
      return state
  }
}
