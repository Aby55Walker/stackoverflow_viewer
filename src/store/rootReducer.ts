import { combineReducers } from 'redux'
import { appReducer } from './appReducers'
import { searchReducer } from './search/searchReducer'
import { modalPostsReducer } from './modalPosts/modalPostReducer'
import { questionPageReducer } from './questionPage/questionPageReducer'

export interface RootState {
  [key: string]: any
}

export default combineReducers({
  app: appReducer,
  search: searchReducer,
  posts: modalPostsReducer,
  question: questionPageReducer,
})
