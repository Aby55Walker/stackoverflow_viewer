import { FETCH_QUESTION_DATA } from '../actionTypes'
import { IAction } from '../types/actionTypes'

const initialState: any = {
  question: {},
  answers: [],
}

export const questionPageReducer = (state = initialState, action: IAction) => {
  switch (action.type) {
    case FETCH_QUESTION_DATA:
      return {
        ...state,
        question: action.payload.question,
        answers: action.payload.answers,
      }
    default:
      return state
  }
}
