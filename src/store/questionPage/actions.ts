import { IAction, IQuestion } from '../types/actionTypes'
import Api from '../../api/fetcher'
import { FETCH_QUESTION_DATA } from '../actionTypes'
import { setLoading } from '../actions'

export const setQuestionData = (action: IAction, payload: any) => {
  return {
    type: action.type,
    payload,
  }
}

export const fetchQuestionData = (questionID: number) => {
  return async (dispatch: (action: IAction) => void) => {
    dispatch(setLoading(true))

    try {
      const answersData = await Api.answers(
        `questions/${questionID}/answers?order=desc&sort=activity&site=stackoverflow&filter=!YOL.*gmgDOK200Pbka8MwAe171&key=2SJ*wXuAaD5J1AYVaZgerA((`
      )
      const questionData: IQuestion = await Api.question(
        `questions/${questionID}?order=desc&sort=activity&site=stackoverflow&filter=!9_bDDxJY5&key=2SJ*wXuAaD5J1AYVaZgerA((`
      )
      dispatch({
        type: FETCH_QUESTION_DATA,
        payload: {
          answers: answersData,
          question: questionData,
        },
      })
      dispatch(setLoading(false))
    } catch (e) {
      console.warn(e.message)
      setLoading(false)
    }
  }
}
