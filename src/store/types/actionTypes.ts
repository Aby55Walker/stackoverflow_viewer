import { ThunkDispatch } from 'redux-thunk'
import { RootState } from '../rootReducer'

export interface IAction {
  type: string
  payload: any
}

export interface IPost {
  owner: {
    id: number
    image: string
    name: string
  }
  questionID: number
  title: string
  answerCount: number
  tags: string[]
}

export interface IQuestion {
  body: string
  owner: {
    name: string
  }
  title: string
}

export interface IAnswer {
  body: string
  owner: {
    name: string
  }
}

export type AppDispatch = ThunkDispatch<RootState, any, IAction>
