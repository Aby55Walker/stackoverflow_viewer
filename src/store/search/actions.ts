import { SEARCH, SET_SEARCH_LOADING, SET_SEARCH_VALUE } from '../actionTypes'
import { IAction } from '../types/actionTypes'
import Api from '../../api/fetcher'

export const setSearchValue = (payload: string): IAction => {
  return {
    type: SET_SEARCH_VALUE,
    payload,
  }
}

export const setSearchLoading = (payload: boolean): IAction => {
  return {
    type: SET_SEARCH_LOADING,
    payload,
  }
}

type searchPayload = {
  searchValue: string
  sort?: string
}

export const search = ({ searchValue, sort = 'votes' }: searchPayload) => {
  return async (dispatch: (action: IAction) => void) => {
    try {
      dispatch(setSearchLoading(true))
      const items = await Api.posts(
        `search?order=desc&sort=${sort}&intitle=${searchValue}&site=stackoverflow&filter=!7qeDKpTTwlbi(mdBB*u.S1NX(u)jjQd*dT&key=2SJ*wXuAaD5J1AYVaZgerA((`
      )
      dispatch({
        type: SEARCH,
        payload: items,
      })
      dispatch(setSearchLoading(false))
    } catch (e) {
      console.warn(e.message)
      dispatch(setSearchLoading(false))
    }
  }
}
