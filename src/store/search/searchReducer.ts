import { SEARCH, SET_SEARCH_LOADING, SET_SEARCH_VALUE } from '../actionTypes'
import { IAction } from '../types/actionTypes'

const initialState = {
  searchResults: {},
  searchValue: '',
  loading: false,
}

export const searchReducer = (state = initialState, action: IAction) => {
  switch (action.type) {
    case SET_SEARCH_VALUE:
      return {
        ...state,
        searchValue: action.payload,
      }
    case SEARCH:
      return {
        ...state,
        searchResults: action.payload,
      }
    case SET_SEARCH_LOADING:
      return {
        ...state,
        loading: action.payload,
      }
    default:
      return state
  }
}
