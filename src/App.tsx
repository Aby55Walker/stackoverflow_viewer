import React from 'react'
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom'
import axios from 'axios'
import { TransitionGroup, CSSTransition } from 'react-transition-group'

import './styles/main.scss'

import SearchPage from './views/SearchPage'
import SearchResultsPage from './views/SearchResultsPage'
import PostsModal from './views/PostsModal'
import QuestionInfoPage from './views/QuestionInfoPage'
import Layout from './components/Layout/Layout'

const App: React.FC = () => {
  axios.defaults.baseURL = 'https://api.stackexchange.com/2.2/'

  return (
    <Router>
      <Layout>
        <Route
          render={({ location }) => {
            return (
              <TransitionGroup>
                <CSSTransition
                  key={location.pathname.split('/')[1]}
                  classNames='fade'
                  timeout={300}
                  unmountOnExit
                  appear
                >
                  <Switch location={location}>
                    <Route component={SearchResultsPage} path='/search-results/:query' />
                    <Route component={QuestionInfoPage} path='/question-info/:questionID' />
                    <Route component={SearchPage} path='/' />
                  </Switch>
                </CSSTransition>
              </TransitionGroup>
            )
          }}
        />
      </Layout>

      <PostsModal />
    </Router>
  )
}

export default App
